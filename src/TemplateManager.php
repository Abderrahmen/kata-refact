<?php

class TemplateManager
{
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        list($quote, $user) = $this->extractData($data);
        list($repoQuote, $repoSite, $repoQuoteDestination) = $this->loadDataFromRepositories($quote);

        $replaced = clone($tpl);
        $replaced
            ->setSubject($this->computeText($replaced->getSubject(), $quote, $user, $repoQuote, $repoSite, $repoQuoteDestination))
            ->setContent($this->computeText($replaced->getContent(), $quote, $user, $repoQuote, $repoSite, $repoQuoteDestination))
            ;

        return $replaced;
    }

    /**
     * @param array $data
     * @return array
     */
    private function extractData($data)
    {
        /**
         * @var $quote
         * @var $user
         */
        extract($data);

        return [
            (isset($quote) && $quote instanceof Quote) ? $quote : null,
            (isset($user)  && $user  instanceof User)  ? $user  : ApplicationContext::getInstance()->getCurrentUser()
        ];
    }

    /**
     * @param Quote | null $quote
     * @return array
     */
    private function loadDataFromRepositories($quote)
    {
        return $quote ? [
            QuoteRepository::getInstance()->getById($quote->id),
            SiteRepository::getInstance()->getById($quote->siteId),
            DestinationRepository::getInstance()->getById($quote->destinationId)
        ] :
            [null, null, null];
    }

    /**
     * Replace all needles with appropriate piece of data
     *
     * @param string $text
     * @param Quote | null $quote
     * @param User $user
     * @param Quote | null $repoQuote
     * @param Site | null $repoSite
     * @param Destination | null $repoQuoteDestination
     * @return string
     */
    private function computeText($text, $quote, $user, $repoQuote, $repoSite, $repoQuoteDestination)
    {
        if ($quote)
        {
            $text = str_replace(
                [
                    Needles::DESTINATION_LINK,
                    Needles::SUMMARY_HTML,
                    Needles::SUMMARY,
                    Needles::DESTINATION_NAME,
                ], [
                    $repoSite->url . '/' . $repoQuoteDestination->countryName . '/quote/' . $repoQuote->id,
                    Quote::renderHtml($repoQuote),
                    Quote::renderText($repoQuote),
                    $repoQuoteDestination->countryName,
            ],
                $text
            );
        }

        /**
         * even if we have a single needle to replace for the moment
         * we keep using arrays with str_replace for future ease of use
         */
        if($user) {
            $text = str_replace(
                [Needles::FIRST_NAME],
                [ucfirst(mb_strtolower($user->firstname))],
                $text
            );
        }

        return $text;
    }
}
