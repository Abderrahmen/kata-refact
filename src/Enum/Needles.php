<?php

class Needles
{
    const DESTINATION_LINK = '[quote:destination_link]';
    const SUMMARY_HTML = '[quote:summary_html]';
    const SUMMARY = '[quote:summary]';
    const DESTINATION_NAME = '[quote:destination_name]';

    const FIRST_NAME = '[user:first_name]';
}